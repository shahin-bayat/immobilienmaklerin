module.exports = {
  purge: [],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      backgroundImage: theme => ({
        "showcase-image":
          "linear-gradient(rgba(0, 0, 0, 0.17), rgba(0, 0, 0, 0.17)),url('../images/showcase.png')",
        "detailsuche-image":
          "linear-gradient(rgba(0, 0, 0, 0.17), rgba(0, 0, 0, 0.17)),url('../images/detailsuche2.png')",
        "kontakt-image":
          "linear-gradient(rgba(0, 0, 0, 0.2), rgba(0, 0, 0, 0.2)),url('../images/kontakt.png')",
        "aboutus-image":
          "linear-gradient(to right, rgba(255, 255, 255, 0.79) 0%, rgba(255, 255, 255, 0.79) 50%, transparent 50%),url('../images/uberuns.png')",
        "immobilien-image":
          "linear-gradient(rgba(0, 0, 0, 0.1), rgba(0, 0, 0, 0.1)),url('../images/immobilienverkauf2.png')",
        "immobilien-image--2":
          "linear-gradient(rgba(0, 0, 0, 0.1), rgba(0, 0, 0, 0.1)),url('../images/haus3.png')",
        "image-box-1":
          "linear-gradient(rgba(255, 255, 255, 0.37), rgba(255, 255, 255, 0.37)),url('../images/box-1.png')",
        "bewertung-image":
          "linear-gradient(rgba(255, 255, 255, 0.37), rgba(255, 255, 255, 0.37)),url('../images/bewertung-main.png')",
        "news-image":
          "linear-gradient(rgba(0, 0, 0, 0.1), rgba(0, 0, 0, 0.1)),url('../images/newsbg.png')",
      }),
      colors: {
        gray: {
          1: "#4B4B4B",
          2: "#545454",
          3: "#B2B2B2",
        },
        green: {
          1: "#B8D442",
        },
      },
      height: {
        "10v": "10vh",
        "20v": "20vh",
        "30v": "30vh",
        "40v": "40vh",
        "50v": "50vh",
        "55v": "55vh",
        "60v": "60vh",
        "70v": "70vh",
        "80v": "80vh",
        "90v": "90vh",
        "100v": "100vh",
      },
      minHeight: {
        "10v": "10vh",
        "20v": "20vh",
        "30v": "30vh",
        "40v": "40vh",
        "50v": "50vh",
        "55v": "55vh",
        "60v": "60vh",
        "70v": "70vh",
        "80v": "80vh",
        "90v": "90vh",
        "100v": "100vh",
      },
      width: {
        "10%": "10%",
        "20%": "20%",
        "30%": "30%",
        "40%": "40%",
        "50%": "50%",
        "60%": "60%",
        "70%": "70%",
        "80%": "80%",
        "90%": "90%",
        "250px": "250px",
        "821px": "821px",
        "757px": "757px",
      },
      inset: {
        "1/10": "10%",
        "568px": "568px",
        "175px": "175px",
        "143px": "143px",
        "393px": "393px",
      },
    },
  },
  variants: {
    extend: {
      display: ["group-hover", "first"],
      fontWeight: ["group-hover"],
    },
  },
  plugins: [require("tailwindcss-nested-groups")],
}
